import os
from selfplay import Coach
from checkers.game import Game
from resnet import CheckNet


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__
    def __getstate__(self): return self.__dict__
    def __setstate__(self, d): self.__dict__.update(d)


dir_path = os.path.dirname(os.path.realpath(__file__))
cache = os.path.join(dir_path, 'alphazero_models')

args = dotdict({
    'cuda': False,
    'numIters': 1000,
    'numEps': 8,  # Number of complete self-play games to simulate during a new iteration.
    'tempThreshold': 15,  #
    'updateThreshold':
    0.6,  # During arena playoff, new neural net will be accepted if threshold or more of games are won.
    'maxlenOfQueue':
    200000,  # Number of game examples to train the neural networks.
    'numMCTSSims': 15,  # Number of games moves for MCTS to simulate.
    'cpuct': 1,
    'checkpoint': cache,
    'load_model': False,
    'load_iteration': 0,
    'numItersForTrainExamplesHistory': 20,
    'start_itr': 0
})

if __name__ == "__main__":
    nnet = CheckNet()
    if args.load_model:
        nnet.load_checkpoint(
            args.checkpoint,
            Coach('', '').getCheckpointFile(args.load_iteration)
        )

    c = Coach(nnet, args)
    if args.load_model:
        print("Load trainExamples from file")
        c.loadTrainExamples()
    c.learn()
