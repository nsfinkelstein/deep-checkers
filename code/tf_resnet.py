import tensorflow as tf
import numpy as np


class CheckNet():
    def __init__(self, num_channels=1024, dropout=.25):
        # game params
        # Renaming functions
        Relu = tf.nn.relu
        Tanh = tf.nn.tanh
        BatchNormalization = tf.layers.batch_normalization
        Dropout = tf.layers.dropout
        Dense = tf.layers.dense

        # Neural Net
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.input_boards = tf.placeholder(tf.float32, shape=[2, 2, 8, 4])

            self.dropout = tf.placeholder(tf.float32)
            self.isTraining = tf.placeholder(tf.bool, name="is_training")

            x_image = tf.reshape(self.input_boards, [-1, 4, 8, 4])
            h_conv1 = Relu(BatchNormalization(self.conv2d(x_image, num_channels, 'same'), axis=3, training=self.isTraining))
            h_conv2 = Relu(BatchNormalization(self.conv2d(h_conv1, num_channels, 'same'), axis=3, training=self.isTraining))
            h_conv3 = Relu(BatchNormalization(self.conv2d(h_conv2, num_channels, 'same'), axis=3, training=self.isTraining))
            h_conv4 = Relu(BatchNormalization(self.conv2d(h_conv3, num_channels, 'same'), axis=3, training=self.isTraining))
            h_conv4_flat = tf.reshape(h_conv4, [-1, num_channels * 32])

            s_fc0 = Dropout(Relu(BatchNormalization(Dense(h_conv4_flat, 1024, use_bias=False), axis=1, training=self.isTraining)), rate=dropout)
            hidden = Dropout(Relu(BatchNormalization(Dense(s_fc0, 512, use_bias=False), axis=1, training=self.isTraining)), rate=dropout)

            piaw = tf.Variable(np.random.normal(size=(512, 32)), dtype=tf.float32)
            pibw = tf.Variable(np.random.normal(size=(512, 32)), dtype=tf.float32)
            vw = tf.Variable(np.random.normal(size=(512, 1)), dtype=tf.float32)
            rw = tf.Variable(np.random.normal(size=(512, 1)), dtype=tf.float32)
            hw = tf.Variable(np.random.normal(size=(512, 512)), dtype=tf.float32)
            hw2 = tf.Variable(np.random.normal(size=(512, 512)), dtype=tf.float32)

            ### when given a board image
            pia = tf.matmul(hidden, piaw)
            pib = tf.matmul(hidden, pibw)

            pi = tf.stack([pia, pib], axis=1)
            pi = tf.matmul(tf.transpose(pi, perm=[0, 2, 1]), pi)
            pi = tf.reshape(pi, [-1, 32 * 32])

            self.pi = tf.nn.log_softmax(pi, axis=1)
            self.v = Tanh(tf.matmul(hidden, vw))
            self.reward = tf.matmul(hidden, rw)
            self.hidden = tf.matmul(Relu(tf.matmul(hidden, hw)), hw2)

            ### when given hidden state and action
            self.input_hidden = tf.placeholder(tf.float32, shape=[1, 512])
            self.input_actions = tf.placeholder(tf.float32, shape=[1, 32 * 32])

            pia = tf.matmul(self.input_hidden, piaw)
            pib = tf.matmul(self.input_hidden, pibw)

            pi = tf.stack([pia, pib], axis=1)
            pi = tf.matmul(tf.transpose(pi, perm=[0, 2, 1]), pi)
            pi = tf.reshape(pi, [-1, 32 * 32])

            self.pi_rec = tf.nn.log_softmax(pi, axis=1)
            self.v_rec = Tanh(tf.matmul(self.input_hidden, vw))
            self.reward_rec = tf.matmul(self.input_hidden, rw)
            self.hidden_rec = tf.matmul(Relu(tf.matmul(self.input_hidden, hw)), hw2)

            self.init = tf.global_variables_initializer()
            self.saver = tf.train.Saver(max_to_keep=None)
            self.variables = tf.global_variables()

        print('MADE NETOWRK')

    def conv2d(self, x, out_channels, padding):
        return tf.layers.conv2d(x, out_channels, kernel_size=[3,3], padding=padding, use_bias=False)
