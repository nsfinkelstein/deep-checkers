from resnet import CheckNet
import random
import multiprocessing as mp
from functools import partial
from collections import deque
from itertools import product, chain, starmap
from checkers.game import Game
from gym import Arena
from treesearch import MCTS
import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time, os, sys
from pickle import Pickler, Unpickler
from random import shuffle


class Coach():
    """
    This class executes the self-play + learning. It uses the functions defined
    in Game and NeuralNet. args are specified in main.py.
    """
    def __init__(self, nnet, args):
        self.nnet = nnet
        self.pnet = self.nnet.__class__()
        self.args = args
        self.skipFirstSelfPlay = False  # can be overriden in loadTrainExamples()

    def learn(self):
        """
        Performs numIters iterations with numEps episodes of self-play in each
        iteration. After every iteration, it retrains neural network with
        examples in trainExamples (which has a maximum length of maxlenofQueue).
        It then pits the new neural network against the old one and accepts it
        only if it wins >= updateThreshold fraction of games.
        """

        self.nnet.save_checkpoint(
            folder=self.args.checkpoint,
            filename=self.getCheckpointFile(self.args.load_iteration)
        )

        for i in range(self.args.load_iteration + 1, self.args.load_iteration + self.args.numIters + 1):
            print('------ITER ' + str(i) + '------')

            playgame = partial(executeEpisode,
                                filename=self.getCheckpointFile(i - 1),
                                args=self.args)

            with mp.Pool(mp.cpu_count()) as p:
                episodes = p.map(playgame, range(self.args.numEps))
            # episodes = map(playgame, range(self.args.numEps))

            train_examples = list(chain.from_iterable(episodes))

            shuffle(train_examples)
            self.nnet.train(train_examples)
            self.nnet.save_checkpoint(
                folder=self.args.checkpoint,
                filename=self.getCheckpointFile(i)
            )

    def getCheckpointFile(self, iteration):
        return 'checkpoint_' + str(iteration) + '.pth.tar'


def executeEpisode(episode, filename, args):
    """
    This function executes one episode of self-play, starting with player 1.
    As the game is played, each turn is added as a training example to
    trainExamples. The game is played till the game ends. After the game
    ends, the outcome of the game is used to assign values to each example
    in trainExamples.
    It uses a temp=1 if episodeStep < tempThreshold, and thereafter
    uses temp=0.
    Returns:
        trainExamples: a list of examples of the form (canonicalBoard,pi,v)
                        pi is the MCTS informed policy vector, v is +1 if
                        the player eventually won the game, else -1.
    """
    nnet = CheckNet()
    try:
        nnet.load_checkpoint(folder=args.checkpoint, filename=filename)
    except ValueError:
        print('Could not load, creating new model', os.path.join(args.checkpoint, filename))
        nnet = CheckNet()

    print('Starting episode', episode)
    trainExamples = []
    game = Game()
    episodeStep = 0

    mcts = MCTS(nnet, args)  # reset search tree
    while True:

        episodeStep += 1
        canonicalBoard = game.getCanonicalForm()
        temp = int(episodeStep < args.tempThreshold)

        pi = mcts.getActionProb(game, canonicalBoard, temp=temp)

        trainExamples.append([canonicalBoard, pi, game.whose_turn()])

        action = np.random.choice(len(pi), p=pi)
        action = ((action // 32), (action % 32))
        game = game.move(action)

        if game.is_over():
            if game.move_limit_reached():
                v = {1: -.5, 2: -.5}
            else:
                v = {  # if it's my turn the opponent had a winning move
                    1: -1 if game.whose_turn() == 1 else 1,
                    2: -1 if game.whose_turn() == 2 else 1,
                }
            return [(x[0], x[1], v[x[2]]) for x in trainExamples]
