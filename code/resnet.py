import os
import time
import torch
import torch.nn as nn
import torch.nn.functional as F

from pytorch_classification.utils import Bar, AverageMeter
from itertools import product
import numpy as np
import torch.optim as optim

torch.autograd.set_detect_anomaly(True)

# inspired by:
# https://github.com/suragnair/alpha-zero-general/blob/master/othello/pytorch/OthelloNNet.py
class Net(nn.Module):
    def __init__(self, num_channels=1024, dropout=.25):
        super(Net, self).__init__()
        self.num_channels = num_channels
        self.dropout = dropout

        # params: in channels, out channels, kernel size, stride, padding
        self.conv1 = nn.Conv2d(4, num_channels, 3, 1, 1)
        self.conv2 = nn.Conv2d(num_channels, num_channels, 3, 1, 1)
        self.conv3 = nn.Conv2d(num_channels, num_channels, 3, 1, 1)
        self.conv4 = nn.Conv2d(num_channels, num_channels, 3, 1, 1)

        self.bn1 = nn.BatchNorm2d(num_channels)
        self.bn2 = nn.BatchNorm2d(num_channels)
        self.bn3 = nn.BatchNorm2d(num_channels)
        self.bn4 = nn.BatchNorm2d(num_channels)

        self.fc1 = nn.Linear(num_channels * 32, 1024)
        self.fc_bn1 = nn.BatchNorm1d(1024)

        self.fc2 = nn.Linear(1024, 512)
        self.fc_bn2 = nn.BatchNorm1d(512)

        self.fc3a = nn.Linear(512, 32)
        self.fc3b = nn.Linear(512, 32)

        self.fc4 = nn.Linear(512, 1)

    def forward(self, s):
        s = s.view(-1, 4, 8, 4)
        s = F.relu(self.bn1(self.conv1(s)))
        s = s + F.relu(self.bn2(self.conv2(s)))
        s = s + F.relu(self.bn3(self.conv3(s)))
        s = s + F.relu(self.bn4(self.conv4(s)))
        s = s.view(-1, self.num_channels * 32)

        s = self.fc_bn1(self.fc1(s))
        s = F.dropout(F.relu(s), p=self.dropout, training=self.training)

        s = self.fc_bn2(self.fc2(s))
        s = F.dropout(F.relu(s), p=self.dropout, training=self.training)

        pia = self.fc3a(s)
        pib = self.fc3b(s)
        v = self.fc4(s)

        pi = torch.stack([pia, pib], dim=1)
        pi = torch.matmul(pi.transpose(1, 2), pi).view(-1, 32 * 32)
        pi = F.log_softmax(pi, dim=1)
        return pi, torch.tanh(v)


args = {
    'batch_size': 64,
    'epochs': 1,
    'cuda': False,
}


class CheckNet(object):
    def __init__(self):
        self.nnet = Net()

    def train(self, examples):
        """
        examples: list of examples, each example is of form (board, pi, v)
        """
        optimizer = optim.Adam(self.nnet.parameters())

        for epoch in range(args['epochs']):
            print('EPOCH ::: ' + str(epoch + 1))
            self.nnet.train()
            data_time = AverageMeter()
            batch_time = AverageMeter()
            pi_losses = AverageMeter()
            v_losses = AverageMeter()
            end = time.time()

            bar = Bar('Training Net',
                      max=int(len(examples) / args['batch_size']))
            batch_idx = 0

            while batch_idx < int(len(examples) / args['batch_size']):
                sample_ids = np.random.randint(len(examples), size=args['batch_size'])
                boards, pis, vs = list(zip(*[examples[i] for i in sample_ids]))

                boards = torch.FloatTensor(np.array(boards).astype(np.float64))
                target_pis = torch.FloatTensor(np.array(pis))
                target_vs = torch.FloatTensor(np.array(vs).astype(np.float64))

                # measure data loading time
                data_time.update(time.time() - end)

                # compute output
                out_pi, out_v = self.nnet(boards)

                l_pi = -torch.mean((target_pis.view(-1) * out_pi.view(-1)))
                l_v = torch.mean((target_vs - out_v.view(-1))**2)
                total_loss = l_pi + l_v

                # record loss
                pi_losses.update(l_pi.item(), boards.size(0))
                v_losses.update(l_v.item(), boards.size(0))

                # compute gradient and do SGD step
                optimizer.zero_grad()
                total_loss.backward()
                optimizer.step()

                # measure elapsed time
                batch_time.update(time.time() - end)
                end = time.time()
                batch_idx += 1

                # plot progress
                bar.suffix = '({batch}/{size}) Data: {data:.3f}s | Batch: {bt:.3f}s | Total: {total:} | ETA: {eta:} | Loss_pi: {lpi:.4f} | Loss_v: {lv:.3f}'.format(
                    batch=batch_idx,
                    size=int(len(examples) / args['batch_size']),
                    data=data_time.avg,
                    bt=batch_time.avg,
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    lpi=pi_losses.avg,
                    lv=v_losses.avg,
                )
                bar.next()
            bar.finish()

    def predict(self, board):
        """
        board: np array with board
        """
        board = torch.FloatTensor(board.astype(np.float64))

        self.nnet.eval()
        with torch.no_grad():
            pi, v = self.nnet(board)

        return torch.exp(pi).data.cpu().numpy()[0], v.data.cpu().numpy()[0]

    def save_checkpoint(self,
                        folder='checkpoint',
                        filename='checkpoint.pth.tar'):
        filepath = os.path.join(folder, filename)
        if not os.path.exists(folder):
            print("Checkpoint Directory does not exist! Making directory {}".
                  format(folder))
            os.mkdir(folder)
        else:
            print("Checkpoint Directory exists! ")
        torch.save({
            'state_dict': self.nnet.state_dict(),
        }, filepath)

    def load_checkpoint(self,
                        folder='checkpoint',
                        filename='checkpoint.pth.tar'):
        # https://github.com/pytorch/examples/blob/master/imagenet/main.py#L98
        filepath = os.path.join(folder, filename)
        if not os.path.exists(filepath):
            raise ValueError("No model in path {}".format(filepath))
        map_location = None if args['cuda'] else 'cpu'
        checkpoint = torch.load(filepath, map_location=map_location)
        print(filepath, checkpoint['state_dict']['fc2.bias'][0])
        self.nnet.load_state_dict(checkpoint['state_dict'])
        print(self.nnet.fc2.bias[0])
