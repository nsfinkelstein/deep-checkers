import numpy as np
import os
from functools import partial
import multiprocess as mp
from itertools import combinations
from muzero import SharedStorage, run_mcts, Node, checkers_config as config, \
                    expand_node, add_exploration_noise, Action, Game

from resnet import CheckNet
from treesearch import MCTS
from main import args


def muzero_agent(step):
    config.num_simulations = sims
    network = SharedStorage(config.cache_dir).load_network(step)

    # This is the muzero version of a game
    def turn(game):
        # print(os.getpid(), 'Next move')
        root = Node(0)
        current_observation = game.make_image(-1)
        expand_node(root, game.to_play(), game.legal_actions(),
                    network.initial_inference(current_observation))
        add_exploration_noise(config, root)

        # We then run a Monte Carlo Tree Search using only action sequences and the
        # model learned by the network.
        # print(os.getpid(), 'To run MCTS')
        run_mcts(config, root, game.action_history(), network)
        pi = np.zeros(32 * 32)
        for action, child in root.children.items():
            pi[action.index] = child.visit_count
        pi /= np.sum(pi)

        action = np.random.choice(len(pi), p=pi)

        game.apply(Action(index=int(action)))
        game.store_search_statistics(root)
        return game, pi

    return turn


def alphazero_agent(step):
    args['numMCTSSims'] = sims
    dir_path = os.path.dirname(os.path.realpath(__file__))
    cache = os.path.join(dir_path, 'alphazero_models')
    filename = 'checkpoint_{}.pth.tar'.format(step)
    network = CheckNet()
    network.load_checkpoint(cache, filename)
    print(network.nnet.fc2.bias[0])

    network.nnet.eval()

    mcts = MCTS(network, args)

    # This is the muzero version of a game
    def turn(game):
        pi = mcts.getActionProb(game.game,
                                game.game.getCanonicalForm(),
                                temp=0)
        pi /= np.sum(pi)

        action = np.random.choice(len(pi), p=pi)
        game.apply(Action(index=int(action)))
        return game, pi

    return turn


def play(agent1, a1desc, agent2, a2desc):

    gameplay = partial(game,
                       agent1=agent1,
                       agent2=agent2,
                       a1desc=a1desc,
                       a2desc=a2desc)
    with mp.Pool(8) as p:
        results = p.map(gameplay, range(num_games))

    return results


def game(game_number, agent1, a1desc, agent2, a2desc):
    agent1 = agent1()
    agent2 = agent2()

    names = {
        agent1: a1desc,
        agent2: a2desc,
    }

    pis = {
        agent1: {},
        agent2: {},
    }

    print('Game number:', game_number)
    game = config.new_game()

    # alternate who plays first
    if game_number % 2 == 0:
        players = [agent1, agent2]
    else:
        players = [agent2, agent1]

    game_length = 0
    while not game.terminal():
        player = players[game.game.whose_turn() - 1]
        game, pi = player(game)

        nonzero = pi[np.nonzero(pi)[0]]
        if len(nonzero) > 1:
            pis[player][tuple(nonzero)] = pis[player].get(tuple(nonzero), 0) + 1
        game_length += 1

    winner = game.game.get_winner()
    w = None
    if winner is not None:
        w = players[game.game.get_winner() - 1]
        print('winner!', names[w])

    else:
        print('winner is None')

    return names.get(w, 'no winner'), {names[k]: v for k, v in pis.items()}, game_length


def self_play(r, agent):
    allresults = []
    for s1, s2 in combinations(r, r=2):
        print(
            agent.__name__, 'at step', s1, 'VS', agent.__name__, 'at step', s2
        )

        results = play(
            partial(agent, s1), agent.__name__ + ' step {}'.format(s1),
            partial(agent, s2), agent.__name__ + ' step {}'.format(s2),
        )

        print('results:', [r[0] for r in results])
        allresults.append(results)
    return allresults


def interplay(r):
    allresults = []
    for s in r:
        print('alpha zero V mu zero at step', s)

        results = play(
            partial(muzero_agent, s), 'muzero agent {}'.format(s),
            partial(alphazero_agent, s), 'alphazero agent {}'.format(s),
        )

        print('results:', [r[0] for r in results])
        allresults.append(results)

    return allresults


def alex_exp():
    allresults = []
    for s in (0, 20, 40, 60, 76):

        np.random.seed(s)
        results = play(
            partial(muzero_agent, s), 'muzero agent {}'.format(s),
            partial(alphazero_agent, 0), 'alphazero agent {}'.format(s),
        )

        print('results:', [r[0] for r in results])
        allresults.append(results)

    return allresults


if __name__ == '__main__':
    import pickle
    sims = 45
    num_games = 16
    alphazero_selfplay = (0, 70, 140, 210, 346)
    muzero_selfplay = (0, 20, 40, 60, 76)

    # with open('results/alphazero_selfplay.pickle', 'wb') as f:
    #     pickle.dump(self_play(alphazero_selfplay, alphazero_agent), f)

    # with open('results/muzero_selfplay.pickle', 'wb') as f:
    #     pickle.dump(self_play(muzero_selfplay, muzero_agent), f)

    # with open('results/interplay.pickle', 'wb') as f:
    #     pickle.dump(interplay(muzero_selfplay), f)

    with open('results/interplay_muzero.pickle', 'wb') as f:
        pickle.dump(alex_exp(), f)
