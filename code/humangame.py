from checkers.game import Game
import numpy as np


def play(agent1, agent2):
    """ returns the winning agent """

    game = Game()
    rep = board_rep(game)

    while not game.is_over():

        if game.whose_turn() == 1:
            try:
                rep, game = make_move(game, rep, agent1)
            except ValueError:
                return agent2

        if game.whose_turn() == 2:
            try:
                rep, game = make_move(game, rep, agent2)
            except ValueError:
                return agent1

    return agent1 if game.get_winner() == 1 else agent2


def make_move(game, rep, agent):
    move = agent(rep, game=game)
    game = game.move(move)
    return board_rep(game), game


def board_rep(game):
    rep = np.zeros((2, 2, 8, 4), dtype=np.int8)

    for piece in game.board.pieces:
        if piece.captured:
            continue

        pos = piece.position - 1
        player = piece.player - 1
        king = int(piece.king)
        row = pos // 4
        col = pos % 4

        rep[player, king, row, col] = 1

    return rep


def human(rep, game):
    print(game.board.position_layout)
    print([(p.position, p.player) for p in game.board.pieces])
    print('Possible moves: ', game.get_possible_moves())
    print(rep)
    move = input('Enter move  as "from , to": ')
    return [int(x) for x in move.split(',')]


if __name__ == '__main__':
    print(play(human, human))
