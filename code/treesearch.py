import math
import torch
import numpy as np
from itertools import product
EPS = 1e-8


class MCTS():
    """
    This class handles the MCTS tree.
    """
    def __init__(self, nnet, args):
        self.nnet = nnet
        self.args = args
        self.Qsa = {}  # stores Q values for s,a (as defined in the paper)
        self.Nsa = {}  # stores #times edge s,a was visited
        self.Ns = {}  # stores #times board s was visited
        self.Ps = {}  # stores initial policy (returned by neural net)

        self.Es = {}  # stores game.getGameEnded ended for board s
        self.Vs = {}  # stores game.getValidMoves for board s

    def getActionProb(self, game, canonicalBoard, temp=1):
        """
        This function performs numMCTSSims simulations of MCTS starting from
        canonicalBoard.
        Returns:
            probs: a policy vector where the probability of the ith action is
                   proportional to Nsa[(s,a)]**(1./temp)
        """
        s = game.stringRepresentation(canonicalBoard)
        for i in range(self.args.numMCTSSims):
            self.search(game, canonicalBoard)

        counts = np.zeros((32, 32))
        for i, j in product(range(32), repeat=2):
            counts[i, j] = self.Nsa.get((s, (i, j)), 0)

        if temp == 0:
            return counts.flatten() / np.sum(counts)

        if temp != 0 and math.isclose(np.sum(counts**(1./temp)), 0):
            bestA = np.argmax(counts)
            probs = np.zeros(32 * 32)
            probs[bestA] = 1
            return probs.flatten()

        expcounts = counts**(1./temp)
        probs = expcounts / np.sum(expcounts)
        return probs.flatten()

    def search(self, game, canonicalBoard):
        """
        This function performs one iteration of MCTS. It is recursively called
        till a leaf node is found. The action chosen at each node is one that
        has the maximum upper confidence bound as in the paper.
        Once a leaf node is found, the neural network is called to return an
        initial policy P and a value v for the state. This value is propagated
        up the search path. In case the leaf node is a terminal state, the
        outcome is propagated up the search path. The values of Ns, Nsa, Qsa are
        updated.

        NOTE: the return values are the negative of the value of the current
        state. This is done since v is in [-1,1] and if v is the value of a
        state for the current player, then its value is -v for the other player.
        Returns:
            v: the negative of the value of the current canonicalBoard
        """
        assert np.all(game.getCanonicalForm() == canonicalBoard), 'game and board dont match'

        if game.is_over():
            return -1

        s = game.stringRepresentation(canonicalBoard)
        if s not in self.Ps:
            # leaf node
            pis, v = self.nnet.predict(canonicalBoard)
            valids = game.get_possible_moves()

            ps = np.zeros((32, 32))
            for valid in valids:
                idx = (valid[0] * 32) + valid[1]
                ps[valid] = pis[idx]

            self.Ps[s] = ps  # self.Ps[s] * valids  # masking invalid moves
            sum_Ps_s = np.sum(self.Ps[s])

            if sum_Ps_s > 0:
                self.Ps[s] /= sum_Ps_s  # renormalize

            else:
                # if all valid moves were masked
                # make all valid moves equally probable
                for valid in valids:
                    idx = (valid[0] * 32) + valid[1]
                    ps[valid] = 1 / len(valids)

            self.Vs[s] = valids
            self.Ns[s] = 0
            return -v[0]

        valids = self.Vs[s]

        cur_best = -float('inf')
        best_act = -1

        # pick the action with the highest upper confidence bound
        for a in valids:
            if (s, a) in self.Qsa:
                u = self.Qsa[
                    (s, a)] + self.args.cpuct * self.Ps[s][a] * math.sqrt(
                        self.Ns[s]) / (1 + self.Nsa[(s, a)])
            else:
                u = self.args.cpuct * self.Ps[s][a] * math.sqrt(
                    self.Ns[s] + EPS)  # Q = 0 ?

            if u > cur_best:
                cur_best = u
                best_act = a

        a = best_act
        game = game.move(a)
        next_s = game.getCanonicalForm()

        v = self.search(game, next_s)

        if (s, a) in self.Qsa:
            self.Qsa[(s, a)] = (self.Nsa[(s, a)] *
                                self.Qsa[(s, a)] + v) / (self.Nsa[(s, a)] + 1)
            self.Nsa[(s, a)] += 1

        else:
            self.Qsa[(s, a)] = v
            self.Nsa[(s, a)] = 1

        self.Ns[s] += 1
        return -v
